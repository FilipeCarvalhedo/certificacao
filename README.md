# README #

Repositório criado para gravação de firmwares de homologação. 

Depois que clonar o repositório execute o comando de "instalar".

./instalar

Quando acabar a instalação basta executar o programar_ble para gravar o firmware de teste feito para o ble ou o programar_ant para gravar o firmware do ant.

#Ble
./programar_ble

Quando terminar de gravar o ble o led da placa que fica próximo ao botão deve ficar azul.

#Ant
./programar_ant

Quando terminar de gravar o ant o led da placa que fica próximo ao botão deve ficar verde.